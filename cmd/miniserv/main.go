package main

import (
	"github.com/go-http-utils/etag"
	"github.com/gorilla/handlers"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/autokubeops/miniserv/internal"
	"gitlab.com/autokubeops/miniserv/internal/env"
	"gitlab.com/autokubeops/miniserv/pkg/dotenv"
	"gitlab.com/autokubeops/miniserv/pkg/spa"
	"gitlab.com/autokubeops/serverless"
	stdlog "log"
	"net/http"
	"path/filepath"
)

type environment struct {
	// StaticDir defines the location of your applications
	// static files (required)
	StaticDir string `split_words:"true" required:"true"`
	// DotEnv is the path to the .env file that should
	// be converted into an env-config.js file (optional)
	DotEnv string `split_words:"true"`
	// EnvFile is the name of the JS file created
	// from the DotEnv file
	EnvFile string `split_words:"true" default:"env-config.js"`
	// Port defines the HTTP port to run on
	Port int `envconfig:"PORT" default:"8080"`
}

func main() {
	log.Infof("starting miniserv %s-%s (%s)", internal.Version, internal.GitCommit, internal.BuildDate)
	// hijack stdlib logging
	stdlog.SetOutput(log.New().Writer())

	// read environment
	var e environment
	if err := envconfig.Process("srv", &e); err != nil {
		log.WithError(err).Fatal("failed to read environment")
		return
	}

	// static dir must be the first set value
	// so that we serve what was set at build
	// time
	staticDir := env.GetFirst("", func(key string) string {
		return e.StaticDir
	})

	if e.DotEnv != "" {
		// dotEnv configuration must be the last set value
		// so that we allow the user to configure it
		// at runtime
		dotEnv := env.GetLast("", func(key string) string {
			return e.DotEnv
		})
		envFile := env.GetLast("", func(key string) string {
			return e.EnvFile
		})
		log.Infof("parsing dotenv: '%s'", dotEnv)
		if err := dotenv.NewReader(dotEnv, filepath.Join(staticDir, envFile)); err != nil {
			log.WithError(err).Warningf("failed to configure %s - this may cause undefined behaviour", envFile)
		}
	}

	// start the file server
	log.Infof("serving directory: %s", staticDir)

	// configure routing
	router := http.NewServeMux()
	router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	})
	router.Handle("/", http.FileServer(spa.NewFileSystem(http.Dir(staticDir))))

	// start the http server
	serverless.NewBuilder(handlers.CompressHandler(etag.Handler(router, false))).
		WithPrometheus().
		WithPort(e.Port).
		Run()
}
