package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"testing"
	"time"
)

func TestEtag(t *testing.T) {
	port := rand.Intn(50000) + 2000
	_ = os.Setenv("SRV_STATIC_DIR", "./testdata")
	_ = os.Setenv("PORT", strconv.Itoa(port))
	go func() {
		main()
	}()
	time.Sleep(time.Second)
	var etag string
	for i := 0; i < 1000; i++ {
		resp, err := http.Get(fmt.Sprintf("http://localhost:%d/test.txt", port))
		assert.NoError(t, err)
		assert.EqualValues(t, http.StatusOK, resp.StatusCode)
		tag := resp.Header.Get("Etag")
		assert.NotEmpty(t, tag)
		if etag != "" {
			assert.EqualValues(t, etag, tag)
		}
		etag = tag
		_ = resp.Body.Close()
	}
}
