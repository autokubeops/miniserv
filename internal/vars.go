package internal

var (
	Version   = "unknown version"
	GitCommit = "unknown reference"
	BuildDate string
)
