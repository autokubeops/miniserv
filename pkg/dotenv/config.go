package dotenv

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"strings"
)

type Reader struct{}

func NewReader(src, dst string) error {
	r := new(Reader)
	lines, err := r.GetLines(src)
	if err != nil {
		return err
	}
	data := r.Parse(lines)
	return r.Write(data, dst)
}

func (*Reader) GetLines(path string) ([]string, error) {
	log.Debugf("reading file: %s", path)
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.WithError(err).Error("failed to read file")
		return nil, err
	}
	return strings.Split(string(data), "\n"), nil
}

func (*Reader) Parse(lines []string) string {
	data := strings.Builder{}
	data.WriteString("window._env_ = {")
	var envCount int
	var count int
	var total int
	for _, l := range lines {
		total++
		bits := strings.SplitN(l, "=", 2)
		if len(bits) != 2 {
			log.Warningf("failed to parse line as key=value: '%s'", l)
			continue
		}
		count++
		key := bits[0]
		value := bits[1]
		envValue := os.Getenv(key)
		if envValue == "" {
			log.Debugf("failed to locate '%s' in environment - using fallback", key)
			envValue = value
		} else {
			envCount++
		}
		data.WriteString(fmt.Sprintf("\n\t%s: \"%s\",", key, envValue))
	}
	data.WriteString("\n};")
	log.Infof("loaded %d/%d variables with %d from the environment", count, total, envCount)
	return data.String()
}

func (*Reader) Write(data, path string) error {
	log.Infof("writing file: %s", path)
	// make sure we write with matching user and group permissions
	// for OpenShift compat
	err := os.WriteFile(path, []byte(data), 0660) //nolint:gosec
	if err != nil {
		log.WithError(err).Error("failed to write to file")
		return err
	}
	return nil
}
