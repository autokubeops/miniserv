PKG=gitlab.com/autokubeops/miniserv
VERSION=$(shell cat version.txt)
GIT_COMMIT?=$(shell git rev-parse --short HEAD)
BUILD_DATE?=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
LDFLAGS?="-X ${PKG}/internal.Version=${VERSION} \
	-X ${PKG}/internal.GitCommit=${GIT_COMMIT} \
	-X ${PKG}/internal.BuildDate=${BUILD_DATE}"

.PHONY: bin/miniserv
bin/miniserv:
	CGO_ENABLED=0 go build -ldflags ${LDFLAGS} -o bin/miniserv ./cmd/...

run: bin/miniserv
	bin/miniserv

package: bin/miniserv
	cd bin; tar -cf miniserv.tgz miniserv ../LICENSE